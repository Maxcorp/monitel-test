<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Parser;
use phpQuery;

class SiteController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $parser = new Parser();
        //$data = $parser->parseData();

        return $this->render('index', [
            //'data'=>$data,
        ]);
    }

    public function actionNewsById()
    {
        if(!is_numeric(Yii::$app->request->get('id'))) {
            throw new NotFoundHttpException();
        }
        $id = intval(Yii::$app->request->get('id'));

        $files_path = Yii::getAlias('@webroot/news.json');
        $data = json_decode(file_get_contents($files_path));

        $result = false;

        if(isset($data[$id])) {
            $result = [
                'date'=>$data[$id]->date,
                'name'=>$data[$id]->name,
                'link'=>$data[$id]->link,
                'img'=>$data[$id]->img,
                'description'=>$data[$id]->description,
            ];
        }

        return \yii\helpers\Json::encode(['result'=>$result]);
    }

    public function actionUpdateNews()
    {
        $parser = new Parser();
        $data = $parser->parseData();

        return \yii\helpers\Json::encode($data);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
