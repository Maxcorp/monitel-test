<table>
    <thead>
    <tr>
        <th data-bind="sort: { arr: Records, prop: 'Name' }">Name</th>
        <th data-bind="sort: { arr: Records, prop: 'Number' }">Number</th>
        <th>Description</th>
    </tr>
    </thead>
    <tbody data-bind="foreach: Records">
    <tr>
        <td data-bind="text: Name"></td>
        <td data-bind="text: Number"></td>
        <td data-bind="text: Desc"></td>
    </tr>
    </tbody>
</table>

<div></div>

<script>
    ko.bindingHandlers.sort = {
        init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var asc = false;
            element.style.cursor = 'pointer';

            element.onclick = function(){

                if(asc) {
                    console.log(document.getElementsByTagName('th'));
                    this.classList.remove("desc_sort");
                    this.classList.add("asc_sort");
                } else {
                    this.classList.remove("asc_sort");
                    this.classList.add("desc_sort");
                }

                console.log(asc);

                var value = valueAccessor();
                var prop = value.prop;
                var data = value.arr;

                asc = !asc;

                data.sort(function(left, right){
                    var rec1 = left;
                    var rec2 = right;

                    if(!asc) {
                        rec1 = right;
                        rec2 = left;
                    }

                    var props = prop.split('.');
                    for(var i in props){
                        var propName = props[i];
                        var parenIndex = propName.indexOf('()');
                        if(parenIndex > 0){
                            propName = propName.substring(0, parenIndex);
                            rec1 = rec1[propName]();
                            rec2 = rec2[propName]();
                        } else {
                            rec1 = rec1[propName];
                            rec2 = rec2[propName];
                        }
                    }

                    return rec1 == rec2 ? 0 : rec1 < rec2 ? -1 : 1;
                });
            };
        }
    };

    function ViewModel(){
        var self = this;
        self.Records = ko.observableArray();
    }

    var viewModel = new ViewModel();
    var recs = [];
    $.getJSON("/web/data.json", function(data) {
        //console.log(data);
        recs.push({Name: 'RRRRR', Number: 555, Desc: 123});

        viewModel.Records(data);
        ko.applyBindings(viewModel);
    })


    /*recs.push({Name: 'Bob', Number: 1, Desc: 123});
    recs.push({Name: 'Joe', Number: 2, Desc: 55});
    recs.push({Name: 'Mitch', Number: 3, Desc: 555});
    recs.push({Name: 'Steven', Number: 4, Desc: 123});
    recs.push({Name: 'Robin', Number: 6, Desc: 12436});
    recs.push({Name: 'Batman', Number: 7, Desc: 87686});
    recs.push({Name: 'John', Number: 5, Desc: 145});*/
    //viewModel.Records(recs);
    //ko.applyBindings(viewModel);

</script>

<style>
    th.desc_sort:before {
        content: "\2191";
    }

    th.asc_sort:before {
        content: "\2193";
    }
</style>