<div>
    <h1>Последние новости</h1>
</div>

<table class="basic-table scroll-table table table-striped table-bordered news-table" id="news-table">
    <thead>
    <tr>
        <th data-bind="sort: { arr: Records, prop: 'date' }">Дата</th>
        <th>Изображение</th>
        <th data-bind="sort: { arr: Records, prop: 'name' }">Название</th>
        <th>Краткое описание</th>
        <th>Ссылка</th>
    </tr>
    </thead>
    <tbody data-bind="foreach: Records">
    <tr data-bind="clickHandler: { click: $parent.selectRowHandler, dblclick: $parent.showNews }, attr:{data: id}, css: { myClass: selected_col }">
        <td data-bind="text: date"></td>
        <td>
            <img data-bind="attr:{src: img}" alt="" width="100px">
        </td>
        <td data-bind="text: name"></td>
        <td data-bind="text: description.substring(0, 120)+'...'"></td>
        <td>
            <a data-bind="attr:{href: link}" target="_blank">Подробнее</a>
        </td>
    </tr>
    </tbody>
</table>

<div class="btn-container">
    <button class="btn-update-news" data-bind="click: updateNews">Обновить</button>
</div>



<div id="myModal" class="modal">
    <div class="modal-content">
        <span class="close" data-bind="click: closeModal">&times;</span>
        <div>
            <span class="modal-date"></span>
            <h1 class="modal-title"></h1>
        </div>
        <div>
            <img src="" alt="" class="modal-img">
            <div class="modal-description"></div>
        </div>
    </div>
</div>


<div class="preloader-container">
    <div class="preloader-container-content">
        <div class="loader"></div>
    </div>
</div>