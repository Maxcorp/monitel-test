<?php
namespace app\models;

use Yii;
use phpQuery;

class Parser
{
    public $url = 'https://m.lenta.ru/';
    public $file_name = 'news.json';

    public function parseData()
    {
        $file_path = Yii::getAlias('@webroot/'.$this->file_name);

        $url = file_get_contents($this->url);
        $pq = phpQuery::newDocument($url);
        $elem = $pq->find('.b-list_top-7 .b-list-item_news .b-list-item__link');

        $i = 0;
        foreach ($elem as $item) {
            $pqLink = pq($item);
            $link = self::cleanData($pqLink->attr('href'));

            //Среди последних новостей могут быть ссылки на сторонние сайты. По ТЗ нам нужны, только lenta.ru
            if (strripos($link, 'lenta.ru') === false) {

                $data[$i]['id'] = $i;
                $data[$i]['link'] = 'https://lenta.ru'.$link;

                $url = file_get_contents($this->url.$link);
                $pq = phpQuery::newDocument($url);
                $img = $pq->find('.g-picture')->attr('src');
                $date = $pq->find('.b-list-item__time .g-time')->attr('datetime');
                $data[$i]['name'] = self::cleanData($pq->find('.b-topic__title')->html());

                $data[$i]['img'] = $img;
                $date = explode(',', $date);
                $data[$i]['date'] = $date[1].", ".$date[0];
                $data[$i]['description'] = $description = self::cleanData($pq->find('.b-topic__body p'));

                $i++;
            }
        }

        file_put_contents($file_path, json_encode($data));

        return $data;
    }

    private static function cleanData(string $item): string
    {
        return trim(strip_tags($item));
    }
}