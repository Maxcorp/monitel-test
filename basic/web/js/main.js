'use strict'
let modal = document.getElementById("myModal");
let table = document.getElementById('news-table');
let cells = table.getElementsByTagName('td');
this.selected_col = ko.observable('');
let dataNews = [];
// myObservableArray = ko.observableArray();

ko.bindingHandlers.sort = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        let asc = false;
        element.style.cursor = 'pointer';

        //console.log(valueAccessor);

        element.onclick = function(){
            if(asc) {
                let th = document.getElementsByTagName('th');
                for (let i = 0; i < th.length; i++) {
                    th[i].classList.remove('desc_sort', 'asc_sort');
                }

                this.classList.remove("desc_sort");
                this.classList.add("asc_sort");
            } else {
                let th = document.getElementsByTagName('th');
                for (let i = 0; i < th.length; i++) {
                    th[i].classList.remove('desc_sort', 'asc_sort');
                }

                this.classList.remove("asc_sort");
                this.classList.add("desc_sort");
            }

            let value = valueAccessor();
            let prop = value.prop;
            let data = value.arr;

            asc = !asc;

            data.sort(function(left, right){
                let rec1 = left;
                let rec2 = right;

                if(!asc) {
                    rec1 = right;
                    rec2 = left;
                }

                let props = prop.split('.');
                for(let i in props){
                    let propName = props[i];
                    let parenIndex = propName.indexOf('()');
                    if(parenIndex > 0){
                        propName = propName.substring(0, parenIndex);
                        rec1 = rec1[propName]();
                        rec2 = rec2[propName]();
                    } else {
                        rec1 = rec1[propName];
                        rec2 = rec2[propName];
                    }
                }

                return rec1 == rec2 ? 0 : rec1 < rec2 ? -1 : 1;
            });
        };
    }
};

function dynamicSort(property, sort_type='desc_sort') {
    let sort_order = 1;
    if(sort_type == 'asc_sort') {
        sort_order = -1;
    }
    if(property[0] === "-") {
        sort_order = -1;
        property = property.substr(1);
    }
    return function (a, b) {
        let result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sort_order;
    }
}

function ViewModel(){
    fetch(`/web/news.json`)
        .then(response => {
            return response.json()
        })
        .then(data => {
            if(data) {
                viewModel.Records(data);

                ko.applyBindings(viewModel);
                dataNews = data;

            } else {
                alert('Error');
            }
        })
        .catch(err => {
            //alert('Error');
            console.log(err);
    });
    
    let self = this;
    self.Records = ko.observableArray();
    console.log(self.Records);

    self.updateNews = function() {
        let update_button = document.querySelector('.btn-update-news');
        let preloader = document.querySelector(".preloader-container");

        preloader.style.display = "block";

        fetch(`/web/index.php?r=site/update-news`)
            .then(response => {
                return response.json()
            })
            .then(data => {
                if(data) {
                    preloader.style.display = "none";

                    let param = null;
                    let sort_type = '';
                    let date_sort = document.getElementsByTagName('th')[0].getAttribute('class');
                    let name_sort = document.getElementsByTagName('th')[2].getAttribute('class');

                    console.log(date_sort + ' ' + name_sort);
                    if(date_sort) {
                        param = 'date';
                        sort_type = date_sort;
                    }

                    if(name_sort) {
                        param = 'name';
                        sort_type = name_sort;
                    }

                    if(param) {
                        data.sort(dynamicSort(param, sort_type));
                    }

                    viewModel.Records(data);
                    update_button.removeAttribute("disabled");

                } else {
                    alert('Error');
                }
            })
            .catch(err => {
               console.log(err);
        });



        return false;
    }
    
    self.closeModal = function () {
        modal.style.display = "none";
    }

    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
}

ko.bindingHandlers.clickHandler= {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        let singleHandler = valueAccessor().click;
        let doubleHandler = valueAccessor().dblclick;
        let delay = valueAccessor().delay || 200;
        let clicks = 0;

        element.addEventListener('click', function (e) {
            let id = element.getAttribute('data');

            //document.querySelector('.date-sorter').click();

            clicks++;
            if (clicks === 1) {
                setTimeout(() => {
                    // Если, сделали один клик
                    if (clicks === 1) {


                        for (let row = 0; row < table.getElementsByTagName('tr').length; row++) {
                            table.getElementsByTagName('tr')[row].classList.remove('selected-row')
                        }
                        document.querySelector(`tr[data="${id}"]`).className += "selected-row";

                        if (singleHandler !== undefined) {
                            singleHandler.call(viewModel, bindingContext.$data, e);
                        }
                    } else {
                        modal.style.display = "block";

                        fetch(`/web/index.php?r=site/news-by-id&id=${id}`)
                            .then(response => {
                                return response.json()
                            })
                            .then(data => {
                                if(data.result) {
                                    //console.log(data.result)
                                    document.querySelector('.modal-date').innerHTML = data.result.date;
                                    document.querySelector('.modal-title').innerHTML = data.result.name;
                                    document.querySelector('.modal-description').innerHTML = data.result.description;
                                    document.querySelector('.modal-img').src = data.result.img;
                                } else {
                                    alert('Error');
                                }
                            })
                            .catch(err => {
                                alert('Error');
                            });

                        if (doubleHandler !== undefined) {
                            doubleHandler.call(viewModel, bindingContext.$data, e);
                        }
                    }
                    clicks = 0;
                }, delay);
            }
        }, false);
    }
};

let viewModel = new ViewModel();