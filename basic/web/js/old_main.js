'use strict'
let modal = document.getElementById("myModal");
let table = document.getElementById('news-table');
let cells = table.getElementsByTagName('td');

ko.bindingHandlers.sort = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var asc = false;
        element.style.cursor = 'pointer';

        element.onclick = function(){

            if(asc) {
                let th = document.getElementsByTagName('th');
                for (let i = 0; i < th.length; i++) {
                    th[i].classList.remove('desc_sort', 'asc_sort');
                }

                this.classList.remove("desc_sort");
                this.classList.add("asc_sort");
            } else {
                let th = document.getElementsByTagName('th');
                for (let i = 0; i < th.length; i++) {
                    th[i].classList.remove('desc_sort', 'asc_sort');
                }

                this.classList.remove("asc_sort");
                this.classList.add("desc_sort");
            }

            console.log(asc);

            var value = valueAccessor();
            var prop = value.prop;
            var data = value.arr;

            asc = !asc;

            data.sort(function(left, right){
                var rec1 = left;
                var rec2 = right;

                if(!asc) {
                    rec1 = right;
                    rec2 = left;
                }

                var props = prop.split('.');
                for(var i in props){
                    var propName = props[i];
                    var parenIndex = propName.indexOf('()');
                    if(parenIndex > 0){
                        propName = propName.substring(0, parenIndex);
                        rec1 = rec1[propName]();
                        rec2 = rec2[propName]();
                    } else {
                        rec1 = rec1[propName];
                        rec2 = rec2[propName];
                    }
                }

                return rec1 == rec2 ? 0 : rec1 < rec2 ? -1 : 1;
            });
        };
    }
};

function ViewModel(){
    var self = this;
    self.Records = ko.observableArray();
}

var viewModel = new ViewModel();

$.getJSON("/web/news.json", function(data) {

    viewModel.Records(data);

    ko.applyBindings(viewModel);

    bindings();
});

let update_button = document.querySelector('.btn-update-news');
update_button.addEventListener('click', () =>{
    update_button.setAttribute("disabled", "disabled");
    let preloader = document.querySelector(".preloader-container");

    preloader.style.display = "block";

    fetch(`/web/index.php?r=site/update-news`)
        .then(response => {
            return response.json()
        })
        .then(data => {
            if(data) {
                preloader.style.display = "none";
                viewModel.Records(data);
                update_button.removeAttribute("disabled");

                bindings();
            } else {
                alert('Error');
            }
        })
        .catch(err => {
            alert('Error');
        });
});


function bindings() {


    for (let i = 0; i < cells.length; i++) {
        let cell = cells[i];

        cell.onclick = function () {
            let rowId = this.parentNode.rowIndex;
            let rowsNotSelected = table.getElementsByTagName('tr');
            for (let row = 0; row < rowsNotSelected.length; row++) {
                rowsNotSelected[row].classList.remove('selected-row');
            }
            let rowSelected = table.getElementsByTagName('tr')[rowId];
            rowSelected.className += "selected-row";
        }

        cell.ondblclick = function () {
            let id = this.parentNode.getAttribute('data');
            modal.style.display = "block";

            fetch(`/web/index.php?r=site/news-by-id&id=${id}`)
                .then(response => {
                    return response.json()
                })
                .then(data => {
                    if(data.result) {
                        //console.log(data.result)
                        document.querySelector('.modal-date').innerHTML = data.result.date;
                        document.querySelector('.modal-title').innerHTML = data.result.name;
                        document.querySelector('.modal-description').innerHTML = data.result.description;
                        document.querySelector('.modal-img').src = data.result.img;
                    } else {
                        alert('Error');
                    }
                })
                .catch(err => {
                    alert('Error');
                });
        }

        let close_modal = document.getElementsByClassName("close")[0];

        close_modal.onclick = function() {
            modal.style.display = "none";
        }

        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    }
}